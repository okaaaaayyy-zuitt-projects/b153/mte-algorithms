let collection = [];

function print() {
    return collection
}

function enqueue() {

}

function dequeue() {


}

function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    if(collection.length == 0) {
        return false;
    }
}


module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty

};